import os
import pathlib
from io import StringIO
from cryptography.fernet import InvalidToken

import config.encryption as encryption
from common import main_path


class Config:
    """Manages application settings, including saving and loading from disk. (ADD EXCEPTIONS)"""
    def __init__(self, file_path: str) -> None:
        self.__credentials = {
            "CLIENT_ID": "",
            "API_KEY": "",
            "API_TOKEN": "",
            "API_REFRESH_TOKEN": "",
            "API_TOKEN_TIMESTAMP": "",
            "API_EXPIRES_IN": ""
        }
        self.__network = {
            "OAUTH_URL": "https://allegro.pl/auth/oauth",
            "REDIRECT_URI": "http://localhost:8000",
            "API_URL": "https://api.allegro.pl"
        }

        self.__sett_groups = {
            "CREDENTIALS": self.__credentials,
            "NETWORK": self.__network
        }
        self.__file_path = file_path
        self.__encr_key = ""

    def load_from_file(self) -> None:
        """Loads settings from disk to memory (REMEMBER TO USE set_password() before calling!)"""
        try:
            with open(self.__file_path, "rb") as f:
                decrypted = encryption.decrypt(f.read(), self.__encr_key)
                input = StringIO(decrypted.decode(encoding="utf-8"))

                for line in input.readlines():
                    split_line = line.split('=', 1)
                    if len(split_line) < 2:
                        continue

                    key = split_line[0]
                    value = split_line[1]
                    for g in self.__sett_groups.items():
                        if key in g[1]:
                            g[1][key] = value.rstrip()
        except FileNotFoundError:
            self.save_to_file()

    def save_to_file(self) -> None:
        """Saves the configuration file to disk."""
        output = StringIO()
        for g in self.__sett_groups.items():
            output.write(f"[{g[0]}]\n")
            for s in g[1].items():
                output.write(f"{s[0]}={s[1]}\n")
            output.write("\n")

        raw_data = output.getvalue().encode(encoding="utf-8")
        encrypted = encryption.encrypt(raw_data, self.__encr_key)

        os.makedirs(os.path.dirname(self.__file_path), exist_ok=True)
        with open(self.__file_path, "wb") as f:
            f.write(encrypted)

    def get(self, sett: str) -> str:
        """Gets the value of a setting with a given key."""
        value = None
        for g in self.__sett_groups.items():
            if sett in g[1]:
                value = g[1][sett]
        return value

    def get_group(self, group: str) -> dict:
        """Gets a copy of a whole group of settings in a dictionary."""
        value = None
        if group in self.__sett_groups:
            value = self.__sett_groups[group].copy()
        return value

    def set(self, sett: str, value: str) -> None:
        """Changes the value of a setting with a given key."""
        for g in self.__sett_groups.items():
            if sett in g[1]:
                g[1][sett] = value

    def empty(self, sett: str) -> bool:
        """Indicates if a setting with a given key is empty."""
        for g in self.__sett_groups.items():
            if sett in g[1]:
                return g[1][sett] == ""

    def exists(self, sett: str) -> bool:
        """Indicates if a setting with a given key exists in the configuration."""
        for g in self.__sett_groups.items():
            if sett in g[1]:
                return True
        return False

    def set_password(self, password: str) -> None:
        """Sets the password used for encryption, must be used before load_from_file()."""
        self.__encr_key = encryption.generate_key(password)

    def check_password(self) -> bool:
        """Checks whether the given password is correct (REMEMBER TO USE set_password() BEFORE CALLING!)"""
        if not os.path.exists(self.__file_path):
            return True
        with open(self.__file_path, "rb") as f:
            try:
                encryption.decrypt(f.read(), self.__encr_key)
                return True
            except InvalidToken:
                return False


config = Config(main_path("config.enc"))
