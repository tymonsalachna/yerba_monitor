from config.config import config


class SessionData:
    """An accessor for config entries related to establishing and maintaining a working API session."""
    def __init__(self) -> None:
        self.oauth_url = ""
        self.client_id = ""
        self.api_key = ""
        self.refresh_token = ""
        self.redirect_uri = ""
        self.api_token = ""
        self.token_timestamp = 0
        self.expires_in = 0

    @property
    def oauth_url(self) -> str:
        return config.get("OAUTH_URL")

    @oauth_url.setter
    def oauth_url(self, value: str):
        config.set("OAUTH_URL", value)

    @property
    def client_id(self) -> str:
        return config.get("CLIENT_ID")

    @client_id.setter
    def client_id(self, value: str):
        config.set("CLIENT_ID", value)

    @property
    def api_key(self) -> str:
        return config.get("API_KEY")

    @api_key.setter
    def api_key(self, value: str):
        config.set("API_KEY", value)

    @property
    def refresh_token(self) -> str:
        return config.get("API_REFRESH_TOKEN")

    @refresh_token.setter
    def refresh_token(self, value: str):
        config.set("API_REFRESH_TOKEN", value)

    @property
    def redirect_uri(self) -> str:
        return config.get("REDIRECT_URI")

    @redirect_uri.setter
    def redirect_uri(self, value: str):
        config.set("REDIRECT_URI", value)

    @property
    def api_token(self) -> str:
        return config.get("API_TOKEN")

    @api_token.setter
    def api_token(self, value: str):
        config.set("API_TOKEN", value)

    @property
    def token_timestamp(self) -> int:
        return int(config.get("API_TOKEN_TIMESTAMP"))

    @token_timestamp.setter
    def token_timestamp(self, value: int):
        config.set("API_TOKEN_TIMESTAMP", str(value))

    @property
    def expires_in(self) -> int:
        return int(config.get("API_EXPIRES_IN"))

    @expires_in.setter
    def expires_in(self, value: int) -> None:
        config.set("API_EXPIRES_IN", str(value))

    @property
    def api_url(self) -> str:
        return config.get("API_URL")

    @api_url.setter
    def api_url(self, value: str) -> None:
        config.set("API_URL", value)


def save() -> None:
    """Saves config to file."""
    config.save_to_file()


session_data = SessionData()
