import base64
import os
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.kdf.pbkdf2 import PBKDF2HMAC
from cryptography.fernet import Fernet, InvalidToken


SALT = b"NA\xd7G\xbc\xb6\xaf\xfba\x90fDZ\xbb\xf4\xc7"


def generate_key(password: str) -> bytes:
    """Generates a SHA256 hashed password used for encryption."""
    pass_enc = password.encode()
    kdf = PBKDF2HMAC(
        algorithm=hashes.SHA256(),
        length=32,
        salt=SALT,
        iterations=100000,
        backend=default_backend()
    )
    return base64.urlsafe_b64encode(kdf.derive(pass_enc))


def encrypt(input: bytes, key: bytes) -> bytes:
    """Encrypts a byte array with a given 256bit key."""
    fernet = Fernet(key)
    return fernet.encrypt(input)


def decrypt(input: bytes, key: bytes) -> bytes:
    """Decrypts a byte array with a given 256bit key."""
    fernet = Fernet(key)
    return fernet.decrypt(input)
