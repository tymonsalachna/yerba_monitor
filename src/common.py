import pathlib
import os
from time import perf_counter


__main_path = os.path.join(pathlib.Path(__file__).parent.absolute(), "..\\")
__start_timer = None
__end_timer = None


def main_path(ext: str = "") -> str:
    """Returns the project's main path. Optionally, appends a path to the main path."""
    return os.path.join(__main_path, ext)


def start_timer() -> None:
    """Starts a precise timer (used for debugging)."""
    global __start_timer
    __start_timer = perf_counter()


def end_timer() -> float:
    """Ends the precise timer and returns the value (used for debugging)."""
    global __end_timer
    __end_timer = perf_counter()
    return timer_elapsed()


def timer_elapsed() -> float:
    """Returns the value of the timer (used for debugging)."""
    global __start_timer, __end_timer
    return __end_timer - __start_timer
