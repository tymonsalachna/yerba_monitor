import json

import common as common
import config.encryption as encryption
import api.session as session
import api.json_structs as js
import api.json_decoders as jd

from config.config import config
from api.request import ItemRequest, RequestInclude


config.set_password("test")
config.load_from_file()

session.connect_to_api()
sess = session.Session()

req = ItemRequest()
req.phrase = "yerba"
req.price_from = 100.5
req.limit = 5
req.fallback = False
req.include = RequestInclude.ONLY_ITEMS

res = sess.get_items(req)
for i in res.items:
    i.print()
