import json

from api.json_structs import Item, Seller, Delivery, Currency, Price, SellingMode, SellingFormat, Category


class Decoder:
    """Base class for all JSON decoders. Should not be used on its own."""
    def __init__(self):
        pass

    def get(self, res_str: str) -> object:
        pass


class ItemDecoder(Decoder):
    """Decoder used for converting a JSON string to a list of items."""
    def __init__(self):
        self.__currency_map = {"PLN": Currency.PLN, "EUR": Currency.EUR}
        self.__selling_format_map = {"BUY_NOW": SellingFormat.BUY_NOW, "AUCTION": SellingFormat.AUCTION}

    def get(self, res_str: str) -> []:
        """Returns a list of decoded items."""
        res = json.loads(res_str)
        promoted_json = res["items"]["promoted"]
        regular_json = res["items"]["regular"]
        ret_list = self.__decode_items(promoted_json)
        ret_list.extend(self.__decode_items(regular_json))
        return ret_list

    def __decode_items(self, items: list):
        """Decodes the main item data."""
        ret_list = []
        for item_json in items:
            seller = self.__get_seller(item_json["seller"])
            delivery = self.__get_delivery(item_json["delivery"])
            selling_mode = self.__get_selling_mode(item_json["sellingMode"])
            images = self.__get_images(item_json["images"])
            category = self.__get_category(item_json["category"])

            item_id = int(item_json["id"])
            item_name = item_json["name"]
            stock = int(item_json["stock"]["available"])

            item = Item(item_id, item_name, seller, delivery, images, selling_mode, stock, category)
            ret_list.append(item)

        return ret_list

    def __get_seller(self, seller_json: dict) -> Seller:
        """Decodes the item's seller data."""
        seller_id = int(seller_json["id"])
        seller_company = seller_json["company"] == "true"
        seller_super = seller_json["superSeller"] == "true"
        return Seller(seller_id, seller_company, seller_super)

    def __get_delivery(self, delivery_json: dict) -> Delivery:
        """Decodes the item's delivery data."""
        delivery_free = delivery_json["availableForFree"] == "true"
        delivery_price_value = float(delivery_json["lowestPrice"]["amount"])
        delivery_price_currency = self.__currency_map[delivery_json["lowestPrice"]["currency"]]
        delivery_price = Price(delivery_price_value, delivery_price_currency)
        return Delivery(delivery_free, delivery_price)

    def __get_selling_mode(self, selling_mode_json: dict) -> SellingMode:
        """Decodes the item's selling mode data."""
        selling_mode_format = self.__selling_format_map[selling_mode_json["format"]]
        selling_mode_price_value = float(selling_mode_json["price"]["amount"])
        selling_mode_price_currency = self.__currency_map[selling_mode_json["price"]["currency"]]
        selling_mode_price = Price(selling_mode_price_value, selling_mode_price_currency)
        selling_mode_popularity = int(selling_mode_json["popularity"])
        return SellingMode(selling_mode_format, selling_mode_price, selling_mode_popularity)

    def __get_images(self, images_json: list) -> list:
        """Decodes the item's image data."""
        images = []
        for ij in images_json:
            images.append(ij["url"])
        return images

    def __get_category(self, category_json: dict) -> Category:
        """Decodes the item's category data (TO BE DETERMINED)."""
        return Category.ANY


item_decoder = ItemDecoder()
