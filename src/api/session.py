import requests
import webbrowser
import json
import time
from http.server import BaseHTTPRequestHandler, HTTPServer

from config.config_data import session_data as data, save as save_data
from common import main_path
from api.json_decoders import Decoder, item_decoder
from api.request import Request, ItemRequest
from api.response import Response, ItemResponse

__connected = False


class NotConnectedToAPIError(Exception):
    """Raised when a session is created without connecting to the API prior."""
    pass


class Session:
    """Class used to abstract sending and receiving data to and from the API (after establishing the connection)."""
    def __init__(self) -> None:
        if not is_connected():
            raise NotConnectedToAPIError
        self.__session = requests.Session()
        self.__set_headers()

    def get_items(self, req: ItemRequest):
        """Executes a request specifically for items and returns a proper response."""
        res = self.__get(req, item_decoder)
        return ItemResponse(res.data, res.success)

    def __get(self, req: Request, decoder: Decoder) -> Response:
        """Executes a generic request and returns a generic response object."""
        url = req.get_url()
        print(f"GET request sent: {url}")
        response_str = self.__session.get(url).text
        response_obj = decoder.get(response_str)
        return Response(response_obj, True)

    def __set_headers(self) -> None:
        """Sets appropriate headers for the session to work."""
        headers = {}
        headers['charset'] = 'utf-8'
        headers['Accept-Language'] = 'pl-PL'
        headers['Content-Type'] = 'application/json'
        headers['Api-Key'] = data.api_key
        headers['Accept'] = 'application/vnd.allegro.public.v1+json'
        headers['Authorization'] = f"Bearer {data.api_token}"
        self.__session.headers.update(headers)

    def __del__(self) -> None:
        self.__session.close()


def connect_to_api() -> None:
    """Connects the application to the API, which allows for a session to be created."""
    global __connected, data

    if data.api_token == "":
        __set_api_token()

    token_url = data.oauth_url + '/token'
    access_token_data = {'grant_type': 'refresh_token',
                         'api-key': data.api_key,
                         'refresh_token': data.refresh_token,
                         'redirect_uri': data.redirect_uri}
    response = requests.post(url=token_url,
                             auth=requests.auth.HTTPBasicAuth(data.client_id, data.api_key),
                             data=access_token_data)
    response_json = json.loads(response.text)

    data.api_token = response_json["access_token"]
    data.refresh_token = response_json["refresh_token"]
    data.token_timestamp = round(time.time())
    data.expires_in = int(response_json["expires_in"])
    save_data()

    __connected = True


def is_connected() -> bool:
    """Returns a value indicating whether a connection to the API has been established."""
    return __connected


def __set_api_token() -> None:
    """Generates an API token if it's not already generated or if it expired."""
    global data

    auth_url = '{}/authorize' \
               '?response_type=code' \
               '&client_id={}' \
               '&api-key={}' \
               '&redirect_uri={}'.format(data.oauth_url, data.client_id, data.api_key, data.redirect_uri)

    parsed_redirect_uri = requests.utils.urlparse(data.redirect_uri)
    server_address = parsed_redirect_uri.hostname, parsed_redirect_uri.port

    class AllegroAuthHandler(BaseHTTPRequestHandler):
        def __init__(self, request, address, server):
            super().__init__(request, address, server)

        def do_GET(self):
            self.send_response(200, 'OK')
            self.send_header('Content-Type', 'text/html')
            self.end_headers()

            self.server.path = self.path
            self.server.access_code = self.path.rsplit('?code=', 1)[-1]

    webbrowser.open(auth_url)
    httpd = HTTPServer(server_address, AllegroAuthHandler)
    httpd.handle_request()
    httpd.server_close()

    token_url = data.oauth_url + '/token'
    access_token_data = {'grant_type': 'authorization_code',
                         'code': httpd.access_code,
                         'api-key': data.api_key,
                         'redirect_uri': data.redirect_uri}
    response = requests.post(url=token_url,
                             auth=requests.auth.HTTPBasicAuth(data.client_id, data.api_key),
                             data=access_token_data)
    response_json = json.loads(response.text)

    data.api_token = response_json["access_token"]
    data.refresh_token = response_json["refresh_token"]
