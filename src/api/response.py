class Response:
    """Base class for creating response classes used for communication with the API.
    Should not be used on its own."""
    def __init__(self, data: object, success: bool):
        self.data = data
        self.success = success


class ItemResponse(Response):
    """Child of the Response class, used for encapsulating the response data when requesting items."""
    def __init__(self, items: [], success: bool):
        Response.__init__(self, items, success)

    @property
    def items(self):
        return self.data

    @items.setter
    def items(self, value: []):
        self.data = value
