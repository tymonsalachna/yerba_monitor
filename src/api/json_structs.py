import json
from enum import Enum


class Currency(Enum):
    """Enum used for storing currencies of items' prices."""
    PLN = 1,
    EUR = 2


class SellingFormat(Enum):
    """Enum used for storing selling formats of items."""
    BUY_NOW = 1,
    AUCTION = 2


class Category(Enum):
    """Enum used for determining the item category. (probably gonna get dumped)"""
    ANY = 0


class Seller:
    """Used to store the item seller data."""
    def __init__(self, id: int, company: bool, super_seller: bool) -> None:
        self.id = id
        self.company = company
        self.super_seller = super_seller


class Price:
    """Used to store various prices (delivery, item)."""
    def __init__(self, value: int, currency: Currency):
        self.value = value
        self.currency = currency

    def __str__(self) -> str:
        ret_str = f"{self.value}"
        if self.currency == Currency.PLN:
            ret_str += " PLN"
        elif self.currency == Currency.EUR:
            ret_str += " EUR"
        return ret_str


class Delivery:
    """Used to store delivery data."""
    def __init__(self, free: bool, price: Price) -> None:
        self.free = free
        self.price = price


class SellingMode:
    """Used to store data about the selling mode of an item (format, price, etc.)."""
    def __init__(self, format: SellingFormat, price: Price, popularity: int) -> None:
        self.format = format
        self.price = price
        self.popularity = popularity


class Item:
    """Main item class, stores all item data."""
    def __init__(self, id: int, name: str, seller: Seller,
                 delivery: Delivery, images: list,
                 selling_mode: SellingMode, stock: int, category: Category) -> None:
        self.id = id
        self.name = name
        self.seller = seller
        self.delivery = delivery
        self.images = images
        self.selling_mode = selling_mode
        self.stock = stock
        self.category = category

    def print(self) -> None:
        """Prints item details to console, used for debug."""
        def __indent(self, line: str, tab: int) -> str:
            ind_str = ""
            for i in range(tab):
                ind_str += " "
            return f"{ind_str}{line}"

        print("item:")
        print(self.__indent(f"id = {self.id}", 4))
        print(self.__indent(f"name = {self.name}", 4))
        print(self.__indent(f"stock = {self.stock}", 4))
        print(self.__indent(f"category = {self.category}", 4))
        print(self.__indent("seller:", 4))
        print(self.__indent(f"id = {self.seller.id}", 8))
        print(self.__indent(f"company = {self.seller.company}", 8))
        print(self.__indent(f"super_seller = {self.seller.super_seller}", 8))
        print(self.__indent("selling_mode:", 4))
        print(self.__indent(f"format = {self.selling_mode.format}", 8))
        print(self.__indent(f"price = {self.selling_mode.price}", 8))
        print(self.__indent(f"popularity = {self.selling_mode.popularity}", 8))
        print(self.__indent("delivery:", 4))
        print(self.__indent(f"free = {self.delivery.free}", 8))
        print(self.__indent(f"price = {self.delivery.price}", 8))
        print(self.__indent("images:", 4))
        for img in self.images:
            print(self.__indent(f"url = {img}", 8))
