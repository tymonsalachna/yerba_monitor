from enum import Enum

from config.config_data import session_data as data


class RequestInclude(Enum):
    """Enum used for filtering requests."""
    ALL = 0,
    ONLY_ITEMS = 1,
    ONLY_FILTERS = 2


class Request:
    """Base class used for creating requests used for communication with the API.
    Should not be used on its own."""
    def __init__(self, path: str, params: dict) -> None:
        self.path = path
        self.params = params
        self.data = {}

    def get_url(self) -> str:
        ret_str = f"{data.api_url}/{self.path}?"
        for p in self.params.items():
            if p[1] == "":
                continue
            ret_str += f"{p[0]}={p[1]}&"
        return ret_str[:-1]


class ItemRequest(Request):
    """Child class of the Request class, used for requesting a list of items,
    based on specific criteria, from the API."""
    def __init__(self):
        params = {"phrase": "", "price.to": "", "price.from": "",
                  "limit": "", "offset": "", "seller.id": "",
                  "category.id": "", "fallback": "", "include": "", "sort": ""}
        Request.__init__(self, "offers/listing", params)

        # a bit of a special case
        self.__curr_include = RequestInclude.ALL
        self.__include_mapping = {RequestInclude.ALL: "all",
                                  RequestInclude.ONLY_ITEMS: "-all&include=items",
                                  RequestInclude.ONLY_FILTERS: "-all&include=filters"}

    @property
    def phrase(self) -> str:
        return self.params["phrase"]

    @phrase.setter
    def phrase(self, value: str) -> None:
        self.params["phrase"] = value

    @property
    def price_to(self) -> float:
        return float(self.params["price.to"])

    @price_to.setter
    def price_to(self, value: float) -> None:
        self.params["price.to"] = str(value)

    @property
    def price_from(self) -> float:
        return float(self.params["price.from"])

    @price_from.setter
    def price_from(self, value: float) -> None:
        self.params["price.from"] = str(value)

    @property
    def limit(self) -> int:
        return int(self.params["limit"])

    @limit.setter
    def limit(self, value: int) -> None:
        self.params["limit"] = str(value)

    @property
    def offset(self) -> int:
        return int(self.params["offset"])

    @offset.setter
    def offset(self, value: int) -> None:
        self.params["offset"] = str(value)

    @property
    def seller_id(self) -> int:
        return int(self.params["seller.id"])

    @seller_id.setter
    def seller_id(self, value: int) -> None:
        self.params["seller.id"] = str(value)

    @property
    def category_id(self) -> int:
        return int(self.params["category.id"])

    @category_id.setter
    def category_id(self, value: int) -> None:
        self.params["category.id"] = str(value)

    @property
    def fallback(self) -> bool:
        return self.params["fallback"] != "false"

    @fallback.setter
    def fallback(self, value: bool) -> None:
        self.params["fallback"] = str(value).lower()

    @property
    def include(self) -> RequestInclude:
        return self.__curr_include

    @include.setter
    def include(self, value: RequestInclude) -> None:
        self.params["include"] = self.__include_mapping[value]
        self.__curr_include = value

    @property
    def sort(self) -> str:
        return self.params["sort"]

    @sort.setter
    def sort(self, value: str) -> None:
        self.params["sort"] = value
