## Installation
- "cd \<project dir\>"
- "pip install pipenv"
- "git clone https://gitlab.com/tymonsalachna/yerba_monitor ."
- "pipenv install"

## Usage
- "cd \<project dir\>"
- "pipenv shell"
- "python src/main.py"
